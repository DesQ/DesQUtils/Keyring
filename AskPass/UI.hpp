/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Keyring (https://gitlab.com/DesQ/Shell)
 * DesQ Keyring is a Keyring and Wallet Application for DesQ Shell
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include "Global.hpp"

#include <DFKeyring.hpp>

namespace DesQ {
    namespace AskPass {
        class UI;

        enum EntryType {
            TypePassword = 0x81F580,
            TypeNewPassword,
            TypeOtherPassword,
            TypeClearText,
            TypeConfirm,
            TypeUnknown,
        };
    }
}

namespace WQt {
    class LayerSurface;
}

class DesQ::AskPass::UI : public QDialog {
    Q_OBJECT;

    public:
        UI( QString prompt, QString identifier, DesQ::AskPass::EntryType type );

        void show();
        void hide();

    private:
        void createUI();
        void autoFillPassword();

        void returnPassword();

        QLineEdit *inputLE;
        QCheckBox *storeCB;

        QString mPrompt;
        QString mIdentifier;
        DesQ::AskPass::EntryType mType;

        QPushButton *okBtn;
        QPushButton *cancelBtn;

        QWidget *base;

        DFL::Keyring *keyring;

        WQt::LayerSurface *cls = nullptr;

    protected:
        void paintEvent( QPaintEvent * );

    Q_SIGNALS:
        void done();
};
