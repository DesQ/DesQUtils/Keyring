/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Keyring (https://gitlab.com/DesQ/Shell)
 * DesQ Keyring is a Keyring and Wallet Application for DesQ Shell
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QCommandLineParser>
#include <iostream>

#include <DFApplication.hpp>

#include <wayqt/WayQtUtils.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/LayerShell.hpp>

#include <desq/desq-config.h>
#include <desq/Utils.hpp>

#include "Global.hpp"
#include "UI.hpp"
#include "Utils.hpp"

WQt::LayerShell *lyrShell    = nullptr;
DFL::Settings   *keyringSett = nullptr;

int main( int argc, char *argv[] ) {
    qputenv( "QT_WAYLAND_USE_BYPASSWINDOWMANAGERHINT", QByteArrayLiteral( "1" ) );

    DFL::Application app( argc, argv );

    app.setOrganizationName( "DesQ" );
    app.setApplicationName( "DesQ SSH AskPass" );
    app.setApplicationVersion( PROJECT_VERSION );
    app.setDesktopFileName( "desq-sshaskpass" );

    QCommandLineParser parser;

    parser.setApplicationDescription( QStringLiteral( "DesQ SSH AskPass" ) );
    parser.addVersionOption();
    parser.addHelpOption();

    parser.addPositionalArgument( "prompt", "A prompt for username or password" );

    parser.process( app );

    keyringSett = DesQ::Utils::initializeDesQSettings( "Keyring", "Keyring" );

    if ( parser.positionalArguments().count() ) {
        /** Prompt String */
        QString prompt( parser.positionalArguments().at( 0 ) );

        /** Parse the prompt first. */
        QString identifier;
        DesQ::AskPass::EntryType type = parsePrompt( prompt, identifier );

        /** If a password is requested, try to read it. */
        if ( type == DesQ::AskPass::TypePassword ) {
            QString passwd;

            /** If read was successful, write it out to stdout */
            if ( retrievePassword( identifier, passwd ) ) {
                QTextStream out( stdout );
                out << passwd << "\n";
                return 0;
            }
        }

        WQt::Registry *reg = new WQt::Registry( WQt::Wayland::display() );
        reg->setup();

        lyrShell = reg->layerShell();

        /** Password wasn't requested, or read failed */
        DesQ::AskPass::UI ui( prompt, identifier, type );

        ui.show();
    }

    else {
        parser.showHelp();
        return 1;
    }

    return 0;//app.exec();
}
