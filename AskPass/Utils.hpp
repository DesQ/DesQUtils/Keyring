/**
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Keyring (https://gitlab.com/DesQ/Shell)
 * DesQ Keyring is a Keyring and Wallet Application for DesQ Shell
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include "UI.hpp"

/**
 * Helper function to parse the prompt.
 * prompt        This is the input prompt
 * identifier    Username/KeyName against which a password is stored in some wallet
 * @return       The type of prompt to be shown
 *
 * Future:
 * We aim to provide unified dialogs for
 * (Username,Password) pair, and for password change requests.
 * To be able to handle such requests, we need to be able to add
 * new types, like TypeUserPass, TypePassChange and such. This
 * should not impact the structure of this function, but that is
 * not guaranteed. Changes will be done to suit our needs without
 * any prior intimation.
 */
DesQ::AskPass::EntryType parsePrompt( const QString prompt, QString& identifier );

/**
 * Helper function to retrieve a stored password.
 * We will check what the user defined backend is, and see if the password
 */
bool retrievePassword( QString identifier, QString& password );
