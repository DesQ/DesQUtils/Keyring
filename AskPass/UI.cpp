/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Keyring (https://gitlab.com/DesQ/Shell)
 * DesQ Keyring is a Keyring and Wallet Application for DesQ Shell
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QIcon>

#include <QApplication>

#include <wayqt/WayQtUtils.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/LayerShell.hpp>

#include "Global.hpp"
#include "UI.hpp"

DesQ::AskPass::UI::UI( QString prompt, QString identifier, DesQ::AskPass::EntryType type ) : QDialog() {
    /** Store the prompt string for further use */
    mPrompt     = prompt;
    mIdentifier = identifier;
    mType       = type;

    /** Create the UI */
    createUI();

    int backend = keyringSett->value( "KeyringBackend" );
    keyring = DFL::Keyring::createInstance( (DFL::Keyring::Backend)backend, "DesQAskPass" );
}


void DesQ::AskPass::UI::show() {
    if ( cls != nullptr ) {
        delete cls;
    }

    QDialog::show();

    if ( lyrShell != nullptr ) {
        cls = lyrShell->getLayerSurface( windowHandle(), nullptr, WQt::LayerShell::Top, "DesQAuth" );
        cls->setSurfaceSize( size() );
        cls->setMargins( QMargins( 0, 0, 0, 0 ) );
        cls->setKeyboardInteractivity( WQt::LayerSurface::Exclusive );
        cls->apply();
    }

    QDialog::exec();
}


void DesQ::AskPass::UI::hide() {
    if ( cls != nullptr ) {
        delete cls;
        cls = nullptr;
    }

    return QDialog::hide();
}


void DesQ::AskPass::UI::createUI() {
    /** Label displaying the icon */
    QLabel *iconLbl = new QLabel();

    /** Label displaying the prompt */
    QLabel *promptLbl = new QLabel( mPrompt );

    promptLbl->setSizePolicy( QSizePolicy( QSizePolicy::MinimumExpanding, QSizePolicy::Minimum ) );
    promptLbl->setWordWrap( true );
    promptLbl->setAlignment( Qt::AlignCenter );

    /** Line edit for user input */
    inputLE = new QLineEdit();

    switch ( mType ) {
        case DesQ::AskPass::EntryType::TypeClearText: {
            inputLE->setPlaceholderText( "Username" );
            break;
        }

        case DesQ::AskPass::EntryType::TypeNewPassword:
        case DesQ::AskPass::EntryType::TypeOtherPassword:
        case DesQ::AskPass::EntryType::TypePassword: {
            inputLE->setEchoMode( QLineEdit::Password );
            inputLE->setPlaceholderText( "Password" );
            break;
        }

        default: {
            break;
        }
    }

    connect(
        inputLE, &QLineEdit::returnPressed, [ = ] () {
            returnPassword();
            close();
        }
    );

    /** Checkbox makes sense only for a password entry */
    storeCB = new QCheckBox( "&Store password in the Wallet." );
    storeCB->setChecked( true );

    /** Buttons */
    okBtn = new QPushButton( this );
    okBtn->setIcon( QIcon::fromTheme( "dialog-ok" ) );
    connect(
        okBtn, &QPushButton::clicked, [ = ]() {
            returnPassword();
            close();
        }
    );

    cancelBtn = new QPushButton( this );
    cancelBtn->setIcon( QIcon::fromTheme( "dialog-cancel" ) );
    connect(
        cancelBtn, &QPushButton::clicked, [ = ]() {
            /** Clear the input field */
            inputLE->clear();
            close();
        }
    );

    switch ( mType ) {
        case DesQ::AskPass::EntryType::TypeClearText: {
            okBtn->setText( "&Ok" );
            cancelBtn->setText( "&Cancel" );
            /** Thus far, we handle only usernames in clear text */
            iconLbl->setPixmap( QPixmap( ":/user.png" ).scaled( QSize( 64, 64 ), Qt::KeepAspectRatio, Qt::SmoothTransformation ) );

            break;
        }

        case DesQ::AskPass::EntryType::TypePassword: {
            okBtn->setText( "&Ok" );
            cancelBtn->setText( "&Cancel" );
            iconLbl->setPixmap( QPixmap( ":/password.png" ).scaled( QSize( 64, 64 ), Qt::KeepAspectRatio, Qt::SmoothTransformation ) );

            break;
        }

        case DesQ::AskPass::EntryType::TypeNewPassword: {
            okBtn->setText( "&Ok" );
            cancelBtn->setText( "&Cancel" );
            iconLbl->setPixmap( QPixmap( ":/password.png" ).scaled( QSize( 64, 64 ), Qt::KeepAspectRatio, Qt::SmoothTransformation ) );
            autoFillPassword();

            break;
        }

        case DesQ::AskPass::EntryType::TypeOtherPassword: {
            okBtn->setText( "&Ok" );
            cancelBtn->setText( "&Cancel" );
            iconLbl->setPixmap( QPixmap( ":/password.png" ).scaled( QSize( 64, 64 ), Qt::KeepAspectRatio, Qt::SmoothTransformation ) );

            break;
        }

        case DesQ::AskPass::EntryType::TypeConfirm: {
            okBtn->setText( "&Yes" );
            cancelBtn->setText( "&No" );
            iconLbl->setPixmap( QPixmap( ":/question.png" ).scaled( QSize( 64, 64 ), Qt::KeepAspectRatio, Qt::SmoothTransformation ) );

            break;
        }

        default: {
            okBtn->setText( "&Yes" );
            cancelBtn->setText( "&No" );
            iconLbl->setPixmap( QPixmap( ":/question.png" ).scaled( QSize( 64, 64 ), Qt::KeepAspectRatio, Qt::SmoothTransformation ) );
            break;
        }
    }

    QHBoxLayout *btnLyt = new QHBoxLayout();

    btnLyt->addStretch();
    btnLyt->addWidget( okBtn );
    btnLyt->addWidget( cancelBtn );

    QGridLayout *baseLyt = new QGridLayout();

    baseLyt->addWidget( iconLbl,   0, 0 );
    baseLyt->addWidget( promptLbl, 0, 1 );

    switch ( mType ) {
        case DesQ::AskPass::EntryType::TypeClearText: {
            baseLyt->addWidget( inputLE, 1, 1 );

            break;
        }

        case DesQ::AskPass::EntryType::TypePassword: {
            baseLyt->addWidget( inputLE, 1, 1 );
            baseLyt->addWidget( storeCB, 2, 1 );

            break;
        }

        case DesQ::AskPass::EntryType::TypeNewPassword: {
            baseLyt->addWidget( inputLE, 1, 1 );
            baseLyt->addWidget( storeCB, 2, 1 );

            break;
        }

        case DesQ::AskPass::EntryType::TypeOtherPassword: {
            baseLyt->addWidget( inputLE, 1, 1 );

            break;
        }

        case DesQ::AskPass::EntryType::TypeConfirm: {
            /** Simple confirmation. Nothing to input or store */
            break;
        }

        /** We'll provide the input in case it's necessary: clear text */
        default: {
            baseLyt->addWidget( inputLE, 1, 1 );
            break;
        }
    }

    baseLyt->addLayout( btnLyt, 3, 1 );

    base = new QWidget();
    base->setLayout( baseLyt );
    base->setFixedWidth( 450 );

    QGridLayout *lyt = new QGridLayout();

    lyt->addWidget( base, 0, 0, Qt::AlignCenter );

    setLayout( lyt );

    setAttribute( Qt::WA_TranslucentBackground );
    setFixedSize( qApp->primaryScreen()->size() );

    setWindowFlags( Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint | Qt::BypassWindowManagerHint );
}


void DesQ::AskPass::UI::autoFillPassword() {
}


void DesQ::AskPass::UI::returnPassword() {
    hide();
    qApp->processEvents();

    switch ( mType ) {
        case DesQ::AskPass::EntryType::TypeNewPassword:
        case DesQ::AskPass::EntryType::TypeOtherPassword:
        case DesQ::AskPass::EntryType::TypePassword: {
            /** Get the password string from the lineedit */
            QString passwd = inputLE->text();
            inputLE->clear();

            /** Send the data to the caller */
            QTextStream out( stdout );
            out << passwd << "\n";

            /** Check if we need to store the password */
            if ( storeCB->isChecked() ) {
                /** Empty wallet == use default */
                keyring->createEntry( mIdentifier, passwd.toUtf8(), "", true );

                if ( keyring->error() != DFL::Keyring::NoError ) {
                    QMessageBox::warning(
                        this,
                        "DesQ Keyring | Error saving secrets",
                        "An error was encountered while trying to save the secrets to your keyring:"
                        "<p><center><tt>" + keyring->errorString() + "</tt></center></p>",
                        QMessageBox::Ok
                    );
                }
            }

            break;
        }

        case DesQ::AskPass::EntryType::TypeClearText: {
            /** Get the output string from the lineedit */
            QString output = inputLE->text();

            /** Send the data to the caller */
            QTextStream out( stdout );
            out << output << "\n";

            break;
        }

        default: {
            qWarning() << "Beware: returning input of unknown prompt!!";

            /** Get the output string from the lineedit */
            QString output = inputLE->text();

            /** Send the data to the caller */
            QTextStream out( stdout );
            out << output << "\n";

            break;
        }
    }
}


void DesQ::AskPass::UI::paintEvent( QPaintEvent *pEvent ) {
    QPainter painter( this );

    painter.setRenderHint( QPainter::Antialiasing );

    /** Darkened background */
    painter.save();
    painter.fillRect( rect(), Qt::transparent );
    painter.fillRect( rect(), QColor( 0, 0, 0, 205 ) );
    painter.restore();

    /** Widget background */
    QPalette pltt( palette() );

    painter.save();
    QColor winClr( pltt.color( QPalette::Window ) );

    painter.setPen( QPen( winClr, 1.0 ) );
    winClr.setAlphaF( 0.4 );
    painter.setBrush( winClr );
    painter.drawRoundedRect( QRectF( base->geometry() ), 4.0, 4.0 );
    painter.restore();

    painter.end();

    QDialog::paintEvent( pEvent );
}
