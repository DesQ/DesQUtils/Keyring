/**
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Keyring (https://gitlab.com/DesQ/Shell)
 * DesQ Keyring is a Keyring and Wallet Application for DesQ Shell
 *
 * Much of the code for parsePrompt function was taken from ksshaskpass.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "Global.hpp"
#include "Utils.hpp"

#include <DFKeyring.hpp>

DesQ::AskPass::EntryType parsePrompt( const QString prompt, QString& identifier ) {
    QRegularExpressionMatch  match;
    DesQ::AskPass::EntryType type = DesQ::AskPass::EntryType::TypeUnknown;

    // [sudo] password for cosmos:
    match = QRegularExpression( QStringLiteral( "\\[sudo\\] password for (.*): " ) ).match( prompt );

    if ( match.hasMatch() ) {
        identifier = match.captured( 1 );
        type       = DesQ::AskPass::EntryType::TypeOtherPassword;
        return type;
    }

    // Wallet passwords
    match = QRegularExpression( QStringLiteral( "\\[KWallet\\] Your wallet (.*) is currently closed. Please enter the password to open it: " ) ).match( prompt );

    if ( match.hasMatch() ) {
        identifier = match.captured( 1 );
        type       = DesQ::AskPass::EntryType::TypeOtherPassword;
        return type;
    }

    // openssh sshconnect2.c
    // Case: password for authentication on remote ssh server
    match = QRegularExpression( QStringLiteral( "^(.*@.*)'s password( \\(JPAKE\\))?: $" ) ).match( prompt );

    if ( match.hasMatch() ) {
        identifier = match.captured( 1 );
        type       = DesQ::AskPass::EntryType::TypePassword;
        return type;
    }

    // openssh sshconnect2.c
    // Case: password change request
    //    "Enter (.*@.*)'s old password: "
    //    "Enter (.*@.*)'s new password: "
    //    "Retype (.*@.*)'s old password: "
    match = QRegularExpression( QStringLiteral( "^(Enter|Retype) (.*@.*)'s (old|new) password: $" ) ).match( prompt );

    if ( match.hasMatch() ) {
        identifier = match.captured( 2 );
        type       = DesQ::AskPass::EntryType::TypePassword;

        if ( ( match.captured( 1 ) == "Enter" ) and ( match.captured( 3 ) == "new" ) ) {
            type = DesQ::AskPass::EntryType::TypeNewPassword;
        }

        return type;
    }

    // openssh sshconnect2.c and sshconnect1.c
    // Case: asking for passphrase for a certain keyfile
    match = QRegularExpression( QStringLiteral( "^Enter passphrase for( RSA)? key '(.*)': $" ) ).match( prompt );

    if ( match.hasMatch() ) {
        identifier = match.captured( 2 );
        type       = DesQ::AskPass::EntryType::TypePassword;
        return type;
    }

    // openssh ssh-add.c
    // Case: asking for passphrase for a certain keyfile for the first time => we should try a password from
    // the wallet
    match = QRegularExpression( QStringLiteral( "^Enter passphrase for (.*?)( \\(will confirm each use\\))?: $" ) ).match( prompt );

    if ( match.hasMatch() ) {
        identifier = match.captured( 1 );
        type       = DesQ::AskPass::EntryType::TypePassword;
        return type;
    }

    // openssh ssh-add.c
    // Case: re-asking for passphrase for a certain keyfile => probably we've tried a password from the
    // wallet, no point
    // in trying it again
    match = QRegularExpression( QStringLiteral( "^Bad passphrase, try again for (.*?)( \\(will confirm each use\\))?: $" ) ).match( prompt );

    if ( match.hasMatch() ) {
        identifier = match.captured( 1 );
        type       = DesQ::AskPass::EntryType::TypeNewPassword;
        return type;
    }

    // openssh ssh-pkcs11.c
    // Case: asking for PIN for some token label
    match = QRegularExpression( QStringLiteral( "Enter PIN for '(.*)': $" ) ).match( prompt );

    if ( match.hasMatch() ) {
        identifier = match.captured( 1 );
        type       = DesQ::AskPass::EntryType::TypePassword;
        return type;
    }

    // openssh mux.c
    match = QRegularExpression( QStringLiteral( "^(Allow|Terminate) shared connection to (.*)\\? $" ) ).match( prompt );

    if ( match.hasMatch() ) {
        identifier = match.captured( 2 );
        type       = DesQ::AskPass::EntryType::TypeConfirm;
        return type;
    }

    // openssh mux.c
    match = QRegularExpression( QStringLiteral( "^Open (.* on .*)?$" ) ).match( prompt );

    if ( match.hasMatch() ) {
        identifier = match.captured( 1 );
        type       = DesQ::AskPass::EntryType::TypeConfirm;
        return type;
    }

    // openssh mux.c
    match = QRegularExpression( QStringLiteral( "^Allow forward to (.*:.*)\\? $" ) ).match( prompt );

    if ( match.hasMatch() ) {
        identifier = match.captured( 1 );
        type       = DesQ::AskPass::EntryType::TypeConfirm;
        return type;
    }

    // openssh mux.c
    match = QRegularExpression( QStringLiteral( "^Disable further multiplexing on shared connection to (.*)? $" ) ).match( prompt );

    if ( match.hasMatch() ) {
        identifier = match.captured( 1 );
        type       = DesQ::AskPass::EntryType::TypeConfirm;
        return type;
    }

    // openssh ssh-agent.c
    match = QRegularExpression( QStringLiteral( "^Allow use of key (.*)?\\nKey fingerprint .*\\.$" ) ).match( prompt );

    if ( match.hasMatch() ) {
        identifier = match.captured( 1 );
        type       = DesQ::AskPass::EntryType::TypeConfirm;
        return type;
    }

    // openssh sshconnect.c
    match = QRegularExpression( QStringLiteral( "^Add key (.*) \\(.*\\) to agent\\?$" ) ).match( prompt );

    if ( match.hasMatch() ) {
        identifier = match.captured( 1 );
        type       = DesQ::AskPass::EntryType::TypeConfirm;
        return type;
    }

    // git imap-send.c
    // Case: asking for password by git imap-send
    match = QRegularExpression( QStringLiteral( "^Password \\((.*@.*)\\): $" ) ).match( prompt );

    if ( match.hasMatch() ) {
        identifier = match.captured( 1 );
        type       = DesQ::AskPass::EntryType::TypePassword;
        return type;
    }

    // git credential.c
    // Case: asking for username by git without specifying any other information
    match = QRegularExpression( QStringLiteral( "^Username: $" ) ).match( prompt );

    if ( match.hasMatch() ) {
        identifier = QString();
        type       = DesQ::AskPass::EntryType::TypeClearText;
        return type;
    }

    // git credential.c
    // Case: asking for password by git without specifying any other information
    match = QRegularExpression( QStringLiteral( "^Password: $" ) ).match( prompt );

    if ( match.hasMatch() ) {
        identifier = QString();
        type       = DesQ::AskPass::EntryType::TypeNewPassword;
        return type;
    }

    // git credential.c
    // Case: asking for username by git for some identifier
    match = QRegularExpression( QStringLiteral( "^Username for '(.*)': $" ) ).match( prompt );

    if ( match.hasMatch() ) {
        identifier = match.captured( 1 );
        type       = DesQ::AskPass::EntryType::TypeClearText;
        return type;
    }

    // git credential.c
    // Case: asking for password by git for some identifier
    match = QRegularExpression( QStringLiteral( "^Password for '(.*)': $" ) ).match( prompt );

    if ( match.hasMatch() ) {
        identifier = match.captured( 1 );
        type       = DesQ::AskPass::EntryType::TypePassword;
        return type;
    }

    // Case: username extraction from git-lfs
    match = QRegularExpression( QStringLiteral( "^Username for \"(.*?)\"$" ) ).match( prompt );

    if ( match.hasMatch() ) {
        identifier = match.captured( 1 );
        type       = DesQ::AskPass::EntryType::TypeClearText;
        return type;
    }

    // Case: password extraction from git-lfs
    match = QRegularExpression( QStringLiteral( "^Password for \"(.*?)\"$" ) ).match( prompt );

    if ( match.hasMatch() ) {
        identifier = match.captured( 1 );
        type       = DesQ::AskPass::EntryType::TypePassword;
        return type;
    }

    // Case: password extraction from mercurial, see bug 380085
    match = QRegularExpression( QStringLiteral( "^(.*?)'s password: $" ) ).match( prompt );

    if ( match.hasMatch() ) {
        identifier = match.captured( 1 );
        type       = DesQ::AskPass::EntryType::TypePassword;
        return type;
    }

    // Case: a random password prompt
    match = QRegularExpression( QStringLiteral( "^[.]*(password|Password):[ ]*$" ) ).match( prompt );

    if ( match.hasMatch() ) {
        identifier = match.captured( 1 );
        type       = DesQ::AskPass::EntryType::TypePassword;
        return type;
    }

    // Nothing matched; either it was called by some sort of a script with a custom prompt (i.e. not
    // ssh-add), or
    // strings we're looking for were broken. Issue a warning and continue without identifier.
    qWarning() << "Unable to parse the prompt:" << prompt;

    return type;
}


bool retrievePassword( QString identifier, QString& password ) {
    /** First obtain the KeyringBackend */
    int backend = keyringSett->value( "KeyringBackend" );

    DFL::Keyring *keyring = DFL::Keyring::createInstance( (DFL::Keyring::Backend)backend, "DesQAskPass" );

    /** Empty wallet == use default */
    QByteArray passwd = keyring->retrieveEntry( identifier, QString() );

    password = QString( passwd );

    return ( keyring->error() == DFL::Keyring::NoError );
}
