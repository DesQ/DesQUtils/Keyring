# DesQ Polkit Agent
## PolicyKit Authenticator for DesQ DE

This is a authenticator for PolKit.
This utility is insprired by LXQt PolKit, and ksshaskpass. The code for parsing the prompts was taken from ksshaskpass, and the code for
polkit-based authentication was taken from LXQt Polkit Agent.


### Notes for compiling (Qt5) - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/DesQ/DesQUtils/Keyring.git Keyring`
- Enter the `Keyring` folder
  * `cd Keyring`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


### Dependencies:
* Qt5 (qtbase5-dev, qtbase5-dev-tools)
* Qt
* [libdesq](https://gitlab.com/DesQ/libdesq)
* [DFL::Applications](https://gitlab.com/desktop-frameworks/applications)
* [DFL::Settings](https://gitlab.com/desktop-frameworks/settings)
* [DFL::Keyring](https://gitlab.com/desktop-frameworks/keyring)
* [DFL::WayQt](https://gitlab.com/desktop-frameworks/wayqt)
* polkit-agent-1
* polkit-qt5-1
* libsecret
* gnome-keyring (runtine, optional)
* kwalletd (runtine, optional)


### Known Bugs
* If kwallet is locked, the kwallet unlock dialog can show up below the desq-ssh-askpass screen.


### Upcoming
* Any other feature you request for... :)
