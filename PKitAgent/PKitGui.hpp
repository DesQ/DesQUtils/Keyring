/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Keyring (https://gitlab.com/DesQ/Shell)
 * DesQ Keyring is a Keyring and Wallet Application for DesQ Shell
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QtWidgets>

#include <PolkitQt1/Details>
#include <PolkitQt1/Identity>

#include "User.hpp"

namespace DesQ {
    namespace PKitAgent {
        class Gui;
    }
}

class DesQ::PKitAgent::Gui : public QWidget {
    Q_OBJECT;

    public:
        Gui( QString, QString, QString, const PolkitQt1::Details&, PolkitQt1::Identity::List );
        ~Gui();

        void setPrompt( QString, bool );

        /** Return the current identity as a string */
        QString identity();

        /** Return the current response as a string */
        QString response();

        /** Show information to the user */
        void showInformation( QString );

        /** Show an error to the user */
        void showError( QString );

        /**
         * We will be showing this dialog as a layer-shell
         * using shell integration. We need to make sure the surface
         * get exclusive keyboard focus.
         * Also, ensure the window opens maximized.
         */
        Q_SLOT void show();

        /**
         * The current identity authenticator was changed
         */
        Q_SIGNAL void currentIdentityChanged( const QString& identity );

        /**
         * [Authenticate] was clicked.
         * This button will not be enabled until a password is entered.
         * The string @response is guaranteed to be non-empty.
         */
        Q_SIGNAL void finished( const QString& response );

        /**
         * [Cancel] was clicked.
         * The dialog will be closed, and the session will end.
         */
        Q_SIGNAL void canceled();

    private:
        void createGUI();

        /**
         * Title labels
         * 1. titleIconLbl - A generic lock icon
         * 2. titleTextLbl - "Authentication required to proceed"
         */
        QPointer<QLabel> titleIconLbl;
        QPointer<QLabel> titleTextLbl;

        /**
         * 1. promptIconLbl - Icon provided by pkit-agent
         * 2. promptTextLbl - Prompt text given by pkit-agent
         */
        QPointer<QLabel> promptIconLbl;
        QPointer<QLabel> promptTextLbl;

        /**
         * Two widgets to show the identity.
         * If we have a single identity in the list,
         * we will be using the label. If we have
         * more than one identities, we'll use the
         * combobox.
         */
        QPointer<QLabel> identityLbl;
        QPointer<QComboBox> identityCB;

        /** Password lineedit */
        QPointer<QLineEdit> passwdLE;

        /**
         * Three labels:
         * 1, iconLbl  - show an icon (info or error icon)
         * 2. infoLbl  - accent colored border.
         * 3. errorLbl - red border.
         */
        QPointer<QLabel> iconLbl;
        QPointer<QLabel> infoLbl;
        QPointer<QLabel> errorLbl;

        QPointer<QToolButton> closeInfoBtn;
        QPointer<QWidget> infoBase;

        /** Authenticate and Cancel buttons */
        QPointer<QPushButton> authBtn;
        QPointer<QPushButton> cancelBtn;

        /** Authenticator base widget */
        QPointer<QWidget> base;

    protected:
        void paintEvent( QPaintEvent * );
};
