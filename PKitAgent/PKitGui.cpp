/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Keyring (https://gitlab.com/DesQ/Shell)
 * DesQ Keyring is a Keyring and Wallet Application for DesQ Shell
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QIcon>
#include <QPushButton>
#include "PKitGui.hpp"
#include <unistd.h>

#include <wayqt/LayerShell.hpp>

#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>

DesQ::PKitAgent::Gui::Gui( QString, QString msg, QString icon, const PolkitQt1::Details&, PolkitQt1::Identity::List ids ) : QWidget() {
    /** Create the GUI */
    createGUI();

    /** Generic stuff */
    setWindowIcon( QIcon::fromTheme( icon, QIcon::fromTheme( "desq" ) ) );
    setWindowTitle( "DesQ Polkit Authenticator" );

    /** Set the prompt icon */
    promptIconLbl->setPixmap( QIcon::fromTheme( icon, QIcon( icon ) ).pixmap( 64, 64 ) );

    /** Set the prompt label */
    promptTextLbl->setText( "<p><b>" + msg + "</b></p>" );

    /** Add the identities to the combobox/label */
    if ( ids.count() == 1 ) {
        identityCB->hide();

        User usr( ids[ 0 ].toString() );
        identityLbl->setText( "Authenticating as: " + QString( "<b>%1 (%2)</b>" ).arg( usr.realName() ).arg( usr.userName() ) );

        /** Update this so that we can always query the current identity using ::identtity() */
        identityCB->addItem( QString( "%1 (%2)" ).arg( usr.realName() ).arg( usr.userName() ) );
        identityCB->setItemData( identityCB->count() - 1, ids[ 0 ].toString(), Qt::UserRole + 1 );
        identityCB->setItemData( identityCB->count() - 1, usr.userId(),        Qt::UserRole + 2 );
        identityCB->setCurrentIndex( 0 );

        emit currentIdentityChanged( ids[ 0 ].toString() );
    }

    else {
        identityLbl->setText( "Authenticate as:" );

        for ( PolkitQt1::Identity id: ids ) {
            User usr( id.toString() );
            identityCB->addItem( QString( "%1 (%2)" ).arg( usr.realName() ).arg( usr.userName() ) );
            identityCB->setItemData( identityCB->count() - 1, id.toString(), Qt::UserRole + 1 );
            identityCB->setItemData( identityCB->count() - 1, usr.userId(),  Qt::UserRole + 2 );
        }

        identityLbl->setBuddy( identityCB );

        /** Set the current user as the default index. */
        User usr( geteuid() );
        for ( int i = 0; i < identityCB->count(); i++ ) {
            if ( identityCB->itemData( i, Qt::UserRole + 2 ).toUInt() == usr.userId() ) {
                identityCB->setCurrentIndex( i );
                emit currentIdentityChanged( identityCB->itemData( i, Qt::UserRole + 1 ).toString() );
                break;
            }
        }
    }

    /** In the future, if the user changes the identity, we need to create a new session */
    connect(
        identityCB, &QComboBox::currentIndexChanged, [ this ] ( int idx ) {
            emit currentIdentityChanged( identityCB->itemData( idx, Qt::UserRole + 1 ).toString() );
        }
    );

    /** Focus the Password field */
    passwdLE->setFocus( Qt::OtherFocusReason );
}


DesQ::PKitAgent::Gui::~Gui() {
    /**
     * All our pointers are QPointers
     * Their lifetimes are managed automatically.
     */
}


void DesQ::PKitAgent::Gui::createGUI() {
    titleIconLbl = new QLabel( this );
    titleIconLbl->setPixmap( QIcon::fromTheme( "lock" ).pixmap( 24 ) );
    titleIconLbl->setFixedSize( QSize( 32, 32 ) );

    titleTextLbl = new QLabel( this );
    titleTextLbl->setText( "Authentication required to proceed" );
    titleTextLbl->setFixedHeight( 32 );

    QHBoxLayout *titleLyt = new QHBoxLayout();
    titleLyt->addWidget( titleIconLbl );
    titleLyt->addWidget( titleTextLbl );

    /** The icon and text will be set later */
    promptIconLbl = new QLabel( this );
    promptIconLbl->setFixedSize( QSize( 72, 72 ) );

    promptTextLbl = new QLabel( this );
    promptTextLbl->setFixedHeight( 72 );

    QHBoxLayout *promptLyt = new QHBoxLayout();
    promptLyt->addWidget( promptIconLbl );
    promptLyt->addWidget( promptTextLbl );

    /** Identities will be created and populated later */
    identityLbl = new QLabel( this );
    identityCB  = new QComboBox( this );

    QHBoxLayout *identityLyt = new QHBoxLayout();
    identityLyt->addStretch();
    identityLyt->addWidget( identityLbl );
    identityLyt->addWidget( identityCB );
    identityLyt->addStretch();

    /** The echo mode will be set later */
    passwdLE = new QLineEdit( this );
    passwdLE->setAlignment( Qt::AlignCenter );

    /** We will create this and hide the widgets */
    iconLbl = new QLabel( this );
    iconLbl->setFixedSize( QSize( 32, 32 ) );

    infoLbl = new QLabel( this );
    infoLbl->setFixedHeight( 32 );

    errorLbl = new QLabel( this );
    errorLbl->setFixedHeight( 32 );

    QHBoxLayout *infoLyt = new QHBoxLayout();
    infoLyt->addStretch();
    infoLyt->addWidget( iconLbl );
    infoLyt->addStretch();
    infoLyt->addWidget( infoLbl );
    infoLyt->addStretch();
    infoLyt->addWidget( errorLbl );
    infoLyt->addStretch();

    closeInfoBtn = new QToolButton();
    closeInfoBtn->setIcon( QIcon::fromTheme( "dialog-close" ) );
    closeInfoBtn->setAutoRaise( true );
    closeInfoBtn->setFixedSize( QSize( 32, 32 ) );

    infoBase = new QWidget();
    infoBase->setObjectName( "infoBase" );
    infoBase->setLayout( infoLyt );

    infoBase->hide();

    authBtn = new QPushButton( this );
    authBtn->setIcon( QIcon::fromTheme( "unlock" ) );
    authBtn->setText( "&Authenticate" );
    authBtn->setDisabled( true );

    cancelBtn = new QPushButton( this );
    cancelBtn->setIcon( QIcon::fromTheme( "dialog-close" ) );
    cancelBtn->setText( "&Cancel" );

    QHBoxLayout *btnLyt = new QHBoxLayout();
    btnLyt->addWidget( cancelBtn );
    btnLyt->addStretch();
    btnLyt->addWidget( authBtn );

    QVBoxLayout *baseLyt = new QVBoxLayout();
    baseLyt->setAlignment( Qt::AlignCenter );

    baseLyt->addLayout( titleLyt );
    baseLyt->addLayout( promptLyt );
    baseLyt->addLayout( identityLyt );
    baseLyt->addWidget( passwdLE );
    baseLyt->addWidget( infoBase );
    baseLyt->addLayout( btnLyt );

    base = new QWidget( this );
    base->setLayout( baseLyt );
    base->setMinimumSize( QSize( 480, 180 ) );

    QGridLayout *lyt = new QGridLayout();
    lyt->addWidget( base, 0, 0, Qt::AlignCenter );

    setLayout( lyt );

    /** Window properties */
    setWindowFlags( Qt::FramelessWindowHint );
    setAttribute( Qt::WA_TranslucentBackground );


    /** Connections */
    connect( closeInfoBtn, &QToolButton::clicked, infoBase, &QWidget::hide );
    connect(
        passwdLE, &QLineEdit::textEdited, [ this ] ( const QString& text ) {
            authBtn->setEnabled( text.length() ? true : false );
        }
    );

    connect(
        authBtn, &QToolButton::clicked, [ this ] () {
            setDisabled( true );

            /** Authentication this session */
            emit finished( passwdLE->text() );
        }
    );

    connect(
        cancelBtn, &QToolButton::clicked, [ this ] () {
            /** Close this dialog */
            close();

            /** Cancel this authentication session */
            emit canceled();
        }
    );
}


void DesQ::PKitAgent::Gui::setPrompt( QString text, bool echo ) {
    passwdLE->setPlaceholderText( text.simplified().replace( ":", "" ) );
    passwdLE->setEchoMode( echo ? QLineEdit::Normal : QLineEdit::Password );
}


QString DesQ::PKitAgent::Gui::identity() {
    return identityCB->currentData( Qt::UserRole + 1 ).toString();
}


QString DesQ::PKitAgent::Gui::response() {
    QString response = passwdLE->text();

    passwdLE->setText( QString() );
    return response;
}


void DesQ::PKitAgent::Gui::showInformation( QString infoText ) {
    infoBase->show();
    infoBase->setStyleSheet( "QWidget#infoBase{ border: 1px solid palette(Highlight); border-radius: 3px; }" );

    iconLbl->setPixmap( QIcon::fromTheme( "info" ).pixmap( 24 ) );
    infoLbl->setText( infoText );

    infoLbl->show();
    errorLbl->hide();
}


void DesQ::PKitAgent::Gui::showError( QString errorText ) {
    infoBase->show();

    iconLbl->setPixmap( QIcon::fromTheme( "error" ).pixmap( 24 ) );
    infoBase->setStyleSheet( "QWidget#infoBase{ border: 1px solid red; border-radius: 3px; }" );
    errorLbl->setText( errorText );

    errorLbl->show();
    infoLbl->hide();
}


void DesQ::PKitAgent::Gui::show() {
    /** Maximized */
    QWidget::showMaximized();

    /** We want the window to be shown */
    qApp->processEvents();

    WQt::LayerSurface *lyrSurf = WQt::LayerShell::layerSurfaceForWindow( windowHandle() );

    lyrSurf->setKeyboardInteractivity( WQt::LayerSurface::Exclusive );
    lyrSurf->apply();
}


void DesQ::PKitAgent::Gui::paintEvent( QPaintEvent *pEvent ) {
    QPainter painter( this );

    /** No pen. We're drawing the base */
    painter.setPen( Qt::NoPen );

    /** Black, 18% opacity */
    painter.setBrush( QColor( 0, 0, 0, 46 ) );

    /** Draw the base */
    painter.drawRect( rect() );

    /** We're drawing a rounded border for the auth gui */
    painter.setRenderHint( QPainter::Antialiasing );

    /** Border will have accent color */
    painter.setPen( QPen( palette().color( QPalette::Accent ), 2.0 ) );

    /** Window Base */
    painter.setBrush( palette().color( QPalette::Window ) );

    /** Draw the Base */
    painter.drawRoundedRect( base->geometry().adjusted( 1.0, 1.0, -1.0, -1.0 ), 5.0, 5.0 );

    /** Done painting */
    painter.end();

    /** Paint the widgets */
    QWidget::paintEvent( pEvent );
}
