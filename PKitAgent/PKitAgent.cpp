/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Keyring (https://gitlab.com/DesQ/Shell)
 * DesQ Keyring is a Keyring and Wallet Application for DesQ Shell
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#define POLKIT_AGENT_I_KNOW_API_IS_SUBJECT_TO_CHANGE    1

#include <PolkitQt1/ActionDescription>
#include <PolkitQt1/Agent/Session>
#include <PolkitQt1/Details>
#include <PolkitQt1/Identity>
#include <PolkitQt1/Subject>

#include <QMessageBox>
#include <QDebug>

#include "PKitAgent.hpp"
#include "PKitGui.hpp"

DesQ::PKitAgent::Listener::Listener( QObject *parent ) : PolkitQt1::Agent::Listener( parent ) {
    mAuthGui.reset( nullptr );
    mSession.reset( nullptr );

    mInProgress = false;
    mAuthGui    = 0;

    PolkitQt1::UnixSessionSubject session( getpid() );

    registerListener( session, QStringLiteral( "/org/DesQ/PolicyKit1/AuthenticationAgent" ) );
}


DesQ::PKitAgent::Listener::~Listener() {
}


void DesQ::PKitAgent::Listener::initiateAuthentication( const QString&                   actionId,
                                                        const QString&                   message,
                                                        const QString&                   iconName,
                                                        const PolkitQt1::Details&        details,
                                                        const QString&                   cookie,
                                                        const PolkitQt1::Identity::List& identities,
                                                        PolkitQt1::Agent::AsyncResult    *result ) {
    QMutexLocker locker( &mutex );

    if ( mInProgress ) {
        QMessageBox::information( 0, tr( "PolicyKit Information" ), tr( "Another authentication is in progress. Please try again later." ) );
        return;
    }

    /** Refuse to attempt authentication if no identities are available */
    if ( identities.count() == 0 ) {
        QMessageBox::critical(
            nullptr,
            "DesQ PolicyKit Authenticator",
            "No user to authenticate as. Please check your system configuration."
        );

        /** Mark the process as completed */
        result->setCompleted();

        return;
    }

    mInProgress = true;

    if ( mAuthGui ) {
        mAuthGui.reset( nullptr );
    }

    // Use std::make_unique for exception safety and cleaner code
    mAuthGui = std::make_unique<DesQ::PKitAgent::Gui>( actionId, message, iconName, details, identities );

    connect(
        mAuthGui.get(), &DesQ::PKitAgent::Gui::currentIdentityChanged, [ this ] () {
            if ( mAuthGui->identity() != mCurrentIdentity.toString() ) {
                mCurrentIdentity = PolkitQt1::Identity::fromString( mAuthGui->identity() );
                retryAuthentication();
            }
        }
    );

    connect(
        mAuthGui.get(), &DesQ::PKitAgent::Gui::finished, [ this ] ( const QString& response ) {
            if ( mAuthGui->identity() == mCurrentIdentity.toString() ) {
                mAuthGui->showInformation( "Authenticating..." );
                mSession->setResponse( response );
            }
        }
    );

    /** Cancel the authentication */
    connect(
        mAuthGui.get(), &DesQ::PKitAgent::Gui::canceled, [ this ] () {
            this->mCanceled = true;

            if ( mSession ) {
                /** This will call completed() */
                mSession->cancel();

                /** Disconnect and delete */
                mSession->disconnect();
            }
        }
    );

    /** We have only one user to authenticate as. Create the session */
    mCurrentIdentity = PolkitQt1::Identity::fromString( mAuthGui->identity() );

    mCookie = cookie;
    mResult = result;

    /** Attempt authentication */
    retryAuthentication();
}


void DesQ::PKitAgent::Listener::retryAuthentication() {

    if ( mCurrentIdentity.isValid() ) {
        mSession = std::make_unique<PolkitQt1::Agent::Session>( mCurrentIdentity, mCookie, mResult );

        connect( mSession.get(), &PolkitQt1::Agent::Session::request,   this, &DesQ::PKitAgent::Listener::request );
        connect( mSession.get(), &PolkitQt1::Agent::Session::completed, this, &DesQ::PKitAgent::Listener::completed );
        connect( mSession.get(), &PolkitQt1::Agent::Session::showError, this, &DesQ::PKitAgent::Listener::showError );
        connect( mSession.get(), &PolkitQt1::Agent::Session::showInfo,  this, &DesQ::PKitAgent::Listener::showInfo );

        mSession->initiate();
    }
}


bool DesQ::PKitAgent::Listener::initiateAuthenticationFinish() {
    mInProgress = false;
    return true;
}


void DesQ::PKitAgent::Listener::cancelAuthentication() {
    // dunno what are those for...
    mInProgress = false;
}


void DesQ::PKitAgent::Listener::request( const QString& request, bool echo ) {
    if ( mAuthGui ) {
        mAuthGui->setPrompt( request, echo );

        mAuthGui->show();
    }
}


void DesQ::PKitAgent::Listener::completed( bool gainedAuth ) {
    /** If mCanceled is true, it means we're done! */
    if ( mCanceled ) {
        mInProgress = false;

        if ( mSession ) {
            /** Mark the result as complete */
            mSession->result()->setCompleted();

            /** Disconnect from all connections */
            mSession->disconnect();

            /** Delete the object */
            mSession->deleteLater();
            mSession.release();
        }

        else {
            /** Mark the copy given to us as complete */
            mResult->setCompleted();
        }

        if ( mAuthGui ) {
            mAuthGui->disconnect();
            mAuthGui.reset( nullptr );
        }

        /** Nothing else remains to be done */
        return;
    }

    /** We did not gain authorization */
    if ( !gainedAuth ) {
        /** 3 attempts: our limit */
        if ( mNumTries < 3 ) {
            mSession->deleteLater();
            mSession.release();

            if ( mAuthGui ) {
                mAuthGui->setEnabled( true );
                mAuthGui->showError( "Authentication failed! Please try again." );
            }

            retryAuthentication();
            return;
        }

        else {
            mInProgress = false;

            if ( mSession ) {
                /** Mark the result as complete */
                mSession->result()->setCompleted();

                /** Disconnect from all connections */
                mSession->disconnect();

                /** Delete the object */
                mSession->deleteLater();
                mSession.release();
            }

            else {
                /** Mark the copy given to us as complete */
                mResult->setCompleted();
            }

            if ( mAuthGui ) {
                mAuthGui->disconnect();
                mAuthGui.reset( nullptr );
            }

            /** Nothing else remains to be done */
            return;
        }
    }

    /** Authentication successful */
    else {
        mInProgress = false;

        if ( mSession ) {
            /** Mark the result as complete */
            mSession->result()->setCompleted();

            /** Disconnect from all connections */
            mSession->disconnect();

            /** Delete the object */
            mSession->deleteLater();
            mSession.release();
        }

        else {
            /** Mark the copy given to us as complete */
            mResult->setCompleted();
        }

        if ( mAuthGui ) {
            mAuthGui->close();

            mAuthGui->disconnect();
            mAuthGui.reset( nullptr );
        }

        /** Nothing else remains to be done */
        return;
    }
}


void DesQ::PKitAgent::Listener::showError( const QString& text ) {
    if ( mAuthGui ) {
        mAuthGui->showError( text );
    }

    else {
        QMessageBox::critical( 0, "DesQ PolicyKit Authenticator", text );
    }
}


void DesQ::PKitAgent::Listener::showInfo( const QString& text ) {
    if ( mAuthGui ) {
        mAuthGui->showInformation( text );
    }

    else {
        QMessageBox::information(
            nullptr,
            "DesQ PolicyKit Authenticator",
            text
        );
    }
}
