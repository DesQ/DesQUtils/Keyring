/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This file was originally a part of LXQt.
 * LXQt - a lightweight, Qt based, desktop toolset <https://lxqt.org>
 * Copyright: 2011-2012 Razor team
 * Authors:
 *   Petr Vanek <petr@scribus.info>
 * Original License: LGPL2 or any later version.
 *
 * Suitable modifications have been made for DesQ
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <glib-object.h>
#include <QCommandLineParser>

#include <QDebug>
#include <DFApplication.hpp>

#include "PKitAgent.hpp"

int main( int argc, char *argv[] ) {
    /** We want to start this session */
    qputenv( "QT_WAYLAND_SHELL_INTEGRATION", "desq-layer-shell" );

    /**
     * Perhaps we can add a few env variables here with suitable output information.
     * It will be of any use only if desq-layer-shell is able to pick it up.
     */
    // qputenv( "__DESQ_PKIT_OUTPUT", "eDP-1" );

    DFL::Application app( argc, argv );

    app.setOrganizationName( "DesQ" );
    app.setApplicationName( "DesQ PKit Agent" );
    app.setApplicationVersion( PROJECT_VERSION );
    app.setDesktopFileName( "desq-pkit-agent" );

    app.setQuitOnLastWindowClosed( false );

    /** Simply to add a help option. */
    QCommandLineParser parser;
    parser.setApplicationDescription( QStringLiteral( "DesQ Policykit Authenticator" ) );
    parser.addVersionOption();
    parser.addHelpOption();

    parser.process( app );

    if ( app.isRunning() ) {
        qDebug() << "Another instance of DesQ PKit Agent is running. Aborting...";
        QMessageBox::information(
            nullptr,
            "DesQ PKit Authenticator",
            "Another instance of DesQ PKit Agent is running. Aborting..."
        );

        app.quit();

        return 0;
    }

    if ( app.lockApplication() ) {
        DesQ::PKitAgent::Listener agent;

        return app.exec();
    }

    else {
        qWarning() << "Unable to lock instance. You may face unintended side-effects.";
    }

    return 0;
}
