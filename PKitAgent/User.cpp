/**
 * Copyright 2020-2022 Britanicus <marcusbritanicus@gmail.com>
 * This file is a part of QtGreet project (https://gitlab.com/marcusbritanicus/QtGreet)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QPixmap>
#include <QDebug>

#include <sys/types.h>
#include <pwd.h>

#include "User.hpp"

static inline QString getIcon( QString name ) {
    QString iconPath = QString( "/home/%1/.face" ).arg( name );

    QPixmap pix( iconPath );

    if ( pix.isNull() ) {
        iconPath = QString( "/home/%1/.face.icon" ).arg( name );
        pix      = QPixmap( iconPath );
    }

    if ( pix.isNull() ) {
        iconPath = QString( "/var/lib/AccountsService/icons/%1" ).arg( name );
        pix      = QPixmap( iconPath );
    }

    if ( pix.isNull() ) {
        iconPath = QString( ":/icons/user.png" );
    }

    return iconPath;
}


User::User( uid_t uid ) {
    struct passwd *pwd = getpwuid( uid );

    if ( !pwd ) {
        qWarning() << "Unable to retrieve user info.";
        return;
    }

    mUserInfo = UserInfo{
        pwd->pw_name,
        pwd->pw_uid,
        pwd->pw_gid,
        QString( pwd->pw_gecos ).replace( ",", "" ),
        pwd->pw_dir,
        pwd->pw_shell,
        getIcon( pwd->pw_name )
    };
}


User::User( QString uname ) {
    QString fixedUname = uname.replace( "unix-user:", "" );
    struct passwd *pwd = getpwnam( fixedUname.toUtf8().constData() );

    if ( !pwd ) {
        qWarning() << "Unable to retrieve user info for" << fixedUname.toUtf8().constData();
        return;
    }

    mUserInfo = UserInfo{
        pwd->pw_name,
        pwd->pw_uid,
        pwd->pw_gid,
        QString( pwd->pw_gecos ).replace( ",", "" ),
        pwd->pw_dir,
        pwd->pw_shell,
        getIcon( pwd->pw_name )
    };
}


QString User::userName() {
    return mUserInfo.username;
}


QString User::realName() {
    return mUserInfo.name;
}


uid_t User::userId() {
    return mUserInfo.uid;
}


gid_t User::groupId() {
    return mUserInfo.gid;
}


QString User::icon() {
    return mUserInfo.icon;
}
