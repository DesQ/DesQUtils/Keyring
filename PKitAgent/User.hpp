/**
 * Copyright 2020-2022 Britanicus <marcusbritanicus@gmail.com>
 * This file is a part of QtGreet project (https://gitlab.com/marcusbritanicus/QtGreet)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QList>
#include <QString>

typedef struct user_t {
    QString username;
    uint    uid;
    uint    gid;
    QString name;
    QString homePath;
    QString shell;
    QString icon;
} UserInfo;

class User {
    public:
        /** Get the user info given the uid */
        User( uid_t );

        /** Get the user info given the username */
        User( QString );

        /** Login name */
        QString userName();

        /** Display Name */
        QString realName();

        /** User ID */
        uid_t userId();

        /** Group ID */
        gid_t groupId();

        /** Icon */
        QString icon();

    private:
        UserInfo mUserInfo;
};
