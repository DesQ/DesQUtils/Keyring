/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Keyring (https://gitlab.com/DesQ/Shell)
 * DesQ Keyring is a Keyring and Wallet Application for DesQ Shell
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#define POLKIT_AGENT_I_KNOW_API_IS_SUBJECT_TO_CHANGE    1

#include <PolkitQt1/Agent/Listener>
#include <PolkitQt1/Agent/Session>
#include <PolkitQt1/Details>
#include <PolkitQt1/Identity>

#include <QApplication>
#include <QHash>
#include <QMessageBox>
#include <QMutex>

namespace DesQ {
    namespace PKitAgent {
        class Listener;
        class Gui;
    }
}

class PolicykitAgentGUI;

class DesQ::PKitAgent::Listener : public PolkitQt1::Agent::Listener {
    Q_OBJECT;

    public:
        Listener( QObject *parent = 0 );
        ~Listener();

    private:
        /** Start the authentication process */
        void initiateAuthentication( const QString&, const QString&, const QString&, const PolkitQt1::Details&, const QString&, const PolkitQt1::Identity::List&, PolkitQt1::Agent::AsyncResult * );

        /** Finish the authentication process */
        bool initiateAuthenticationFinish();

        /** Cancel the authentication process */
        void cancelAuthentication();

        /** Perform a request */
        void request( const QString& request, bool echo );

        /** Auth complete; mark session as completed */
        void completed( bool gainedAuthorization );

        /** Show an error to the user */
        void showError( const QString& text );

        /**
         * Show some information to the user.
         * For example, another authentication in progress
         */
        void showInfo( const QString& text );

        /**
         * Helper function: Retry Authentication
         */
        void retryAuthentication();

        std::unique_ptr<DesQ::PKitAgent::Gui> mAuthGui;
        std::unique_ptr<PolkitQt1::Agent::Session> mSession;
        PolkitQt1::Identity mCurrentIdentity;

        /** Since we do not manage the lifetime of this object, we'll use a raw pointer */
        PolkitQt1::Agent::AsyncResult *mResult;
        QString mCookie;

        bool mInProgress = false;
        bool mCanceled   = false;

        int mNumTries = 0;

        QMutex mutex;
};
